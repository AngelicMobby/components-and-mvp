import React from 'react';
import { string } from 'prop-types';

import { ParagraphStyled } from './ParagraphStyled';

const Paragraph = props => <ParagraphStyled {...props} />;

Paragraph.propTypes = {
	size: string
};

Paragraph.defaultProps = {
	size: 'regular'
};

export default Paragraph;
