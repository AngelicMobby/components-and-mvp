import React from 'react';
// import { withRouter } from 'react-router-dom';

import View from '~src/containers/View';
import { getNewScreen, setScreenName } from '~src/helpers/screen.mjs';

import { SectionStyled } from './HomeViewStyled';

class HomeView extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			// Data from backend(s)
			data: [],

			// Screens
			screens: {
				isDisplayingFront: true,
				isDisplayingError: false
			},

			// Dialogs
			dialogs: {
				isDisplayingSignout: false
			},

			// Events
			isLoading: true
		};
	}

	async componentDidMount() {
		// const data = await getData(endpoint, options);

		this.setState({
			// data,
			isLoading: false
		});
	}

	toggleScreen(type, newScreen) {
		/**
		 * Expected format, example:
		 * toggleScreen is called like so: toggleScreen('screens', 'ingredients')
		 * Valid arguments should be 'dialog' (popup) or 'screen'
		 * Arguments will match: this.state.screens.isDisplayingIngredients
		 * NOTE: This function is the same for screens AND dialogs
		 */
		this.setState({
			[type]: getNewScreen(this.state.screens, setScreenName(newScreen))
		});
	}

	render() {
		const { isLoading } = this.state;

		return (
			<View title="Home | PWA by Humblebee">
				<SectionStyled data-test-id="content">
					{isLoading ? <p>Loading</p> : <p>Content goes here</p>}
				</SectionStyled>
			</View>
		);
	}
}

// If you need any information from the router such as match, params or history, use the withRouter HOC.
// export default withRouter(HomeView);
export default HomeView;
