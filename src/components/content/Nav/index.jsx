import React from 'react';
import { Link } from 'react-router-dom';

import { NavStyled } from './NavStyled';

const Nav = (props) => (
	<NavStyled {...props}>
		<Link to="/">Home</Link>
		<Link to="/examples">Examples</Link>
		<Link to="/form">Form</Link>
		<Link to="/some-non-existing-page">404</Link>
	</NavStyled>
);

export default Nav;
