import React from 'react';
import { Redirect, Route } from 'react-router-dom';
import { bool, string } from 'prop-types';

// Alternative1: inject authentication informations as props
// Or an HOC to inject it from a state container?
// export default connect((state) => ({ isAuthenticated: !!state.authentication.token }))(AuthenticatedRoute)
/**
 * Route requiring authentication
 *
 * Will redirect to login path otherwise
 */
const AuthenticatedRoute = ({ isAuthenticated, redirectTo, ...props }) =>
	isAuthenticated
		? <Route {...props} />
		: <Redirect to={redirectTo} />

AuthenticatedRoute.propTypes = {
	isAuthenticated: bool.isRequired,
	redirectTo: string
};

// Alternative2: use a helper to get authentication from wherever
// import { isAuthenticated } from '~src/helpers/authentication';
//
// const AuthenticatedRoute = ({ redirectTo, ...props }) =>
//  isAuthenticated()
// 	 ? <Route ...props />
// 	 : <Redirect to={redirectTo} />
//
// AuthenticatedRoute.propTypes = {
// 	redirectTo: string
// };

AuthenticatedRoute.defaultProps = {
	redirectTo: '/login'
};

export default AuthenticatedRoute;
