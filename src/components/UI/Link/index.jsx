import React from 'react';
import { string } from 'prop-types';

import { LinkStyled } from './LinkStyled';

const Link = ({ to, ...props }) => <LinkStyled href={to} {...props} />;

Link.propTypes = {
	target: string,
	to: string,
	rel: string
};

Link.defaultProps = {
	target: '_self',
	rel: 'noreferrer noopener'
};

export default Link;
