import styled from 'styled-components';

import colors from '~tokens/colors.mjs';
import spacing from '~tokens/spacing.mjs';
import fontSizes from '~tokens/fontSizes.mjs';
import lineHeights from '~tokens/lineHeights.mjs';

import breakpoints from '~src/helpers/breakpoints.mjs';

export const TextareaWrapperStyled = styled.div`
	width: 100%;
	box-sizing: border-box;
	margin-bottom: ${spacing.small};
	font-size: 16px;
`;

export const TextareaStyled = styled.textarea`
	appearance: none;
	width: ${({ inline }) => inline ? 'auto' : '100%'};
	min-height: 150px;
	margin-top: ${spacing.tiny};
	padding: ${spacing.tiny};
	font-size: ${fontSizes.body1};
	line-height: 1;
	box-sizing: border-box;
	font-size: ${fontSizes.body1};
	line-height: ${lineHeights.s};
	border: 0;
	border-radius: 5px;
	border-bottom: 1px solid ${colors.gray3};
	background-color: ${colors.gray5};

	&:focus {
		outline: 0;
	}

	@media screen and (max-width: ${breakpoints.breakpointM}px) {
		font-size: 16px;
	}
`;
