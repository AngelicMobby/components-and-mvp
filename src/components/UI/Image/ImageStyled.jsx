import styled from 'styled-components';

export const ImageWrapperStyled = styled.div`
	overflow: hidden;
	max-width: 100%;
`;

export const ImageStyled = styled.img`
	display: block;
	width: ${({ maxWidth }) => maxWidth ? 'auto' : '100%'};
	max-width: ${({ maxWidth }) => maxWidth ? '100%' : 'none'};
`;
