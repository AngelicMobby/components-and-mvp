import styled from 'styled-components';

import breakpoints from '~src/helpers/breakpoints.mjs';

import colors from '~tokens/colors.mjs';
import spacing from '~tokens/spacing.mjs';
import fontSizes from '~tokens/fontSizes.mjs';
import fontWeights from '~tokens/fontWeights.mjs';

export const SelectWrapperStyled = styled.div`
	width: 100%;
	box-sizing: border-box;
	margin-bottom: ${spacing.small};
	font-size: 16px;
`;

export const SelectStyled = styled.select`
	appearance: none;
	width: ${({ inline }) => inline ? 'auto' : '100%'};
	box-sizing: border-box;
	font-size: 16px;
	margin-top: ${spacing.tiny};
	padding: ${spacing.tiny};
	background-color: ${colors.gray5};
	border: 0;
	border-radius: 5px;
	border-bottom: 1px solid ${colors.gray3};
	background-color: ${colors.gray5};

	&:focus {
		outline: none;
	}

	@media screen and (max-width: ${breakpoints.breakpointM}) {
		font-size: 16px;
	}

	option {
		padding: ${spacing.small} ${spacing.tiny};
		line-height: 1.4em !important;

		&:disabled {
			color: ${colors.gray2};
		}
	}
`;
