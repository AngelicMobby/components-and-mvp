import React from 'react';
import { any, func } from 'prop-types';

import Button from '~src/components/UI/Button';

import { DialogStyled, DialogInsideStyled, DialogContainerStyled } from './DialogStyled';

const Dialog = ({ toggleDialog, children }) => (
	<DialogContainerStyled>
		<DialogStyled>
			<DialogInsideStyled>
				<Button onClick={toggleDialog} />
				{children}
			</DialogInsideStyled>
		</DialogStyled>
	</DialogContainerStyled>
);

Dialog.propTypes = {
	children: any /* eslint-disable-line react/forbid-prop-types */,
	toggleDialog: func.isRequired
};

Dialog.defaultProps = {};

export default Dialog;
