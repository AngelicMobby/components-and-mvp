# A new Humblebee PWA project

## System requirements

- `node`: `>= 8.5.0`
- `yarn`: `*`

## Stack

- Webpack 4
- React 16.7 || Preact 8.2
- Styled Components 4
- Babel 7
- ESLint, Stylelint
- Prettier
- Unistore (optional Redux replacement)
- Jest (optional)

## Commands

All commands are, as usual, available in package.json. Make sure to configure the various variables, such as URLs before doing anything.

- `yarn dev`: Start development server (using `webpack-dev-server`)
- `yarn build`: Output a static build inside `dist` folder

## PWA considerations

Before deploying your application to production, have a look at its SEO / PWA settings.

Please replace the icons and update files within `public` folder:

- `public/index.html`
- `public/robots.txt`
- `public/sitemap.xml`
- `public/assets/manifest.webmanifest`
- `public/assets/manifest.webapp`

## Developing

Open the src folder, and have fun :)

## Deployment

Make sure to update the HTTP headers definition (enable `Content-Security-Policy` and `Feature-Policy`)

## Testing | CI

If you choose the feature `tests`, testing specific dependencies, scripts and configs will be included in your project.

Some default tests will also be injected as an example.

Currently, this feature includes examples for end-to-end and visual regression tests.

Thanks to jest and puppeteer, the tests are predefined to run a production build and run on actual production generated source code.

### End 2 End tests

Root folder: `tests/e2e/containers`

Those tests simulate user behaviour on application pages.

Main purpose: __report potential feature regressions__ to occur after changes.

Scripts:

- `yarn test:e2e`
- `yarn test:e2e:debug` (open a browser and let you visualize puppeteer run your tests)

### Visual regression tests

Root folder: `tests/e2e/visual-regression`

Those tests take screenshots of a given set of application pages, and compare with the previous version.

Main purpose: __report potential visual regressions__ to occur after changes.

Scripts:

- `yarn test:visual`

If you notice a non-matching screenshot (that would actually happen quite often!)

You will need to manually update the screenshots, so the new version become the source of truth for the next comparison.

- `yarn test:visual -u`

### Run tests automatically in a pipeline

If you wish to automate tests in a pipeline, just run `yarn test`, one command to run them all!

The only requirement for puppeteer would be that the local server port matches the base URL (those are defined as environment variables in the ".env" file)

Make sure the following ports matches each other!

```
APP_BASE_URL='http://localhost:3000'
DEV_SERVER_PORT=3000
```

In case you would not be able to update the ".env" file yourself (no access to CI/deployment server), no worries: you can also define those parameters as proper environment variables.
