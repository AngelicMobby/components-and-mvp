import React from 'react';
import { arrayOf, number, object, oneOf, oneOfType, string } from 'prop-types';

import { GridChildStyled } from './GridChildStyled';

// USAGE OF COLUMNSPAN EXAMPLE (12 columns up until 900px then 6):
// columnSpan={[{ columns: 12 }, { break: 900, columns: 6 }]}
const GridChild = props => (
	<GridChildStyled {...props} className={`GridChild ${props.className || ''}`} />
);

GridChild.propTypes = {
	className: string,
	columnSpan: oneOfType([number, arrayOf(object)]),
	align: oneOf(['end', 'center', 'start', 'stretch']),
	padding: string,
	rowSpan: string,
	rowStart: string
};

GridChild.defaultProps = {
	align: 'stretch'
};

export default GridChild;
