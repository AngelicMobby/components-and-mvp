export const getNewScreen = (screenState, newScreen) => {
	const screens = {};

	Object.entries(screenState).forEach(item => {
		screens[item[0]] = false;
	});

	screens[newScreen] = true;

	return screens;
};

export const setScreenName = screen => {
	const newScreen = screen.replace(/^\w/, c => c.toUpperCase());
	return `isDisplaying${newScreen}`;
};
