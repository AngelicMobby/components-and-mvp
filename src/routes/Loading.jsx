import React from 'react';
import { bool, object } from 'prop-types';

// TODO: map the following values to proper loader & error components.
const Loading = ({ error, pastDelay }) => {
	if (error) {
		console.error(error); /* eslint-disable-line no-console */
		return <p>Error...</p>;
	} else if (pastDelay) {
		return <p>Timed out...</p>;
	}

	return <p>Loading...</p>;
};

Loading.propTypes = {
	error: object,
	pastDelay: bool
};

Loading.defaultProps = {
	error: undefined,
	pastDelay: false
};

export default Loading;
