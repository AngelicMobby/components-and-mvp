import styled from 'styled-components';

import colors from '~tokens/colors.mjs';

export const NavStyled = styled.nav`
	border-bottom: 1px solid ${colors.gray3};

	a {
		display: inline-flex;
		padding: 1em;
	}
`;
