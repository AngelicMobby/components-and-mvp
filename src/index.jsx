import React from 'react';
import { hydrate, render } from 'react-dom';
import { BrowserRouter } from 'react-router-dom';

import GlobalStyle from '~src/components/content/GlobalStyle';
import Nav from '~src/components/content/Nav';
import Routes from '~src/routes';

const App = () => (
	<BrowserRouter>
		<main id="app">
			<GlobalStyle />
			<Nav />
			<Routes />
		</main>
	</BrowserRouter>
);

/**
 * Launch the app
 *
 * `react-snap` static pages generator will need to hydrate instead of render
 */
const mountElement = document.querySelector('#root');
if (mountElement.hasChildNodes()) {
	hydrate(<App />, mountElement);
} else {
	render(<App />, mountElement);
}

// Set-up service worker (PWA)
if ('serviceWorker' in navigator) {
	window.addEventListener('load', () => {
		navigator.serviceWorker
			.register('/sw.js')
			.then(registration => {
				/* eslint-disable-next-line no-console */
				console.log('SW registered: ', registration);
				// registration.pushManager.subscribe({ userVisibleOnly: true });
			})
			.catch(registrationError => {
				/* eslint-disable-next-line no-console */
				console.warn('Error registering SW!', registrationError);
			});
	});
}
