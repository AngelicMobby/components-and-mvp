import React from 'react';

import View from '~src/containers/View';

import Link from '~src/components/UI/Link';
import Button from '~src/components/UI/Button';
import Image from '~src/components/UI/Image';
import Grid from '~src/components/UI/Grid';
import GridChild from '~src/components/UI/GridChild';

import TestImage from '~src/assets/img/image.jpg';
import TestGraphic from '~src/assets/gfx/graphic.svg';

import breakpoints from '~src/helpers/breakpoints.mjs';

import { SectionStyled } from './ExamplesViewStyled';

const exampleChildren = [
	{ word: 'Wham', bgc: '#E4572E' },
	{ word: 'Boom', bgc: '#76B041' },
	{ word: 'Sploosh', bgc: '#FFC914' }
];

class ExamplesView extends React.Component {
	render() {
		return (
			<View title="Examples | PWA by Humblebee">
				<SectionStyled>
					<h1>Examples View</h1>
					<div>
						<p>
							Thanks for using a Humblebee Boilerplate, installed through the{' '}
							<Link href="https://bitbucket.org/humblebee/humblebee-pwa-builder/">
								Humblebee PWA Builder
							</Link>{' '}
							package.
						</p>
					</div>
					<Grid
						maxWidth="1440px"
						rowGap={40}
						columnGap={10}
						style={{ backgroundColor: '#2E282A' }}
						childPadding="30px"
						margins="60px auto"
					>
						<GridChild style={{ backgroundColor: '#17BEBB' }}>
							<h2>Grid & GridChild example</h2>
							<h4>
								Those h2 and h4 tag are both a part of the same GridChild. This grid is 6 columns
								and 4 columns on mobile.
							</h4>
						</GridChild>
						{exampleChildren.map((soundEffect, index) => (
							<GridChild
								key={index}
								style={{ backgroundColor: soundEffect.bgc }}
								columnSpan={[
									{ start: 1 },
									{
										break: breakpoints.maxMobile,
										columns: 4,
										start: index % 2 === 0 ? 1 : 3
									}
								]}
							>
								<h3>{soundEffect.word}</h3>
								columnSpan: [ &#123; start: 1 &#125;, &#123; break: breakpoints.maxMobile, columns:
								4, start: {index % 2 === 0 ? 1 : 3} &#125; ]
							</GridChild>
						))}
					</Grid>
					<Button>Button</Button>
					<Image src={TestImage} caption="Image alt text and title as caption" />
					<Image src={TestGraphic} caption="Image alt text and title as caption" />
				</SectionStyled>
			</View>
		);
	}
}

export default ExamplesView;
