import React from 'react';
import { bool } from 'prop-types';

import { H1Styled } from './H1Styled';

const H1 = props => <H1Styled {...props} />;

H1.propTypes = {
	center: bool
};

H1.defaultProps = {
	center: false
};

export default H1;
