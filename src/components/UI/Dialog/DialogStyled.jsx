import styled from 'styled-components';

import colors from '~tokens/colors.mjs';
import fontSizes from '~tokens/fontSizes.mjs';

import Box from '~src/components/UI/Box';

export const DialogContainerStyled = styled.div`
	position: fixed;
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
	background-color: rgba(0,0,0,0.8);
`;

export const DialogStyled = styled(Box)`
	width: 100%;
	position: fixed;
	left: 0;
	bottom: 0;
	margin-bottom: 0;
	box-sizing: border-box;

	color: ${colors.black};
	background-color: ${colors.white};
	font-size: ${fontSizes.h2};
	text-align: center;
`;

export const DialogInsideStyled = styled.div`
	width: 100%;
	max-width: 500px;
	margin: 0 auto;
`;
