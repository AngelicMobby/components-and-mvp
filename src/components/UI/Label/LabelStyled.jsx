import styled from 'styled-components';

export const LabelStyled = styled.label`
	width: 100%;
	float: left;
	clear: both;
`;
