import React from 'react';

import View from '~src/containers/View';

import Button from '~src/components/UI/Button';
import Input from '~src/components/UI/Input';
import Textarea from '~src/components/UI/Textarea';
import Select from '~src/components/UI/Select';

import colors from '~tokens/colors.mjs';

import { SectionStyled, StyledPre } from './FormViewStyled';

class FormView extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			isFormSubmitted: false,
			values: {
				firstName: '',
				lastName: '',
				email: 'hello@humblebee.se',
				password: '',
				comment: '',
				decisiveChoice: 'Red pill'
			}
		};
	}

	onFirstnameChange = event => {
		this.setState({
			values: {
				...this.state.values,
				firstName: event.target.value
			}
		});
	};

	onLastnameChange = event => {
		this.setState({
			values: {
				...this.state.values,
				lastName: event.target.value
			}
		});
	};

	onEmailChange = event => {
		this.setState({
			values: {
				...this.state.values,
				email: event.target.value
			}
		});
	};

	onPasswordChange = event => {
		this.setState({
			values: {
				...this.state.values,
				password: event.target.value
			}
		});
	};

	onCommentChange = event => {
		this.setState({
			values: {
				...this.state.values,
				comment: event.target.value
			}
		});
	};

	onDecisiveChoiceChange = event => {
		this.setState({
			values: {
				...this.state.values,
				decisiveChoice: event.target.value
			}
		});
	};

	onSubmit = event => {
		event.preventDefault();

		this.setState({
			isFormSubmitted: true
		});

		// You probably wish to implement some actions with the form values here :)
		console.info(this.state.values); /* eslint-disable-line no-console */
	};

	render() {
		const { isFormSubmitted, values } = this.state;

		return (
			<View title="Form | PWA by Humblebee">
				<SectionStyled>
					<h1>Form example</h1>
					<form name="example-form" onSubmit={this.onSubmit}>
						<Input
							type="text"
							label="First name"
							name="firstName"
							placeholder="First Name"
							value={values.firstName}
							onChange={this.onFirstnameChange}
						/>
						<Input
							type="text"
							label="Last name"
							name="lastName"
							placeholder="Last Name"
							value={values.lastName}
							onChange={this.onLastnameChange}
						/>
						<Input
							type="email"
							label="Email"
							name="email"
							placeholder="Email"
							required
							value={values.email}
							onChange={this.onEmailChange}
						/>
						<Input
							type="password"
							label="Password"
							name="password"
							placeholder="keep this secret!"
							required
							value={values.password}
							onChange={this.onPasswordChange}
						/>
						<Textarea
							label="Some comment?"
							name="comment"
							value={values.comment}
							onChange={this.onCommentChange}
						/>
						<Select
							label="Are you ready to enter the rabbit hole, Neo?"
							name="decisiveChoice"
							value={values.decisiveChoice}
							onChange={this.onDecisiveChoiceChange}
							style={{
								color:
									values.decisiveChoice === 'Red pill'
										? colors.red
										: values.decisiveChoice === 'Blue pill'
										? colors.blue
										: undefined
							}}
						>
							<option value="Red pill" style={{ color: colors.red }}>
								Take the red pill - Remember, all I offer is truth, nothing more!
							</option>
							<option value="Blue pill" style={{ color: colors.blue }}>
								Take the blue pill - Forget about everything, sweet dreams forever...
							</option>
							<option value="Green pill" disabled>
								There is no such thing as a green pill, WTF?!
							</option>
						</Select>
						<Button type="submit">Submit</Button>
					</form>
					<StyledPre data-test-id="feedback" submitted={isFormSubmitted}>
						{JSON.stringify(
							{
								submitted: isFormSubmitted,
								values
							},
							null,
							2
						)}
					</StyledPre>
				</SectionStyled>
			</View>
		);
	}
}

export default FormView;
