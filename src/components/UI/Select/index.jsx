import React from 'react';
import { bool, string } from 'prop-types';

import Label from '~src/components/UI/Label';

import { SelectWrapperStyled, SelectStyled } from './SelectStyled';

const Select = ({ label, name, ...props }) => (
	<SelectWrapperStyled>
		<Label htmlFor={name}>{label}</Label>
		<SelectStyled id={name} name={name} {...props} />
	</SelectWrapperStyled>
);

Select.propTypes = {
	label: string,
	name: string.isRequired,
	required: bool
};

Select.defaultProps = {
	label: 'Some choice',
	required: false
};

export default Select;
