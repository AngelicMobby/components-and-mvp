import React from 'react';
import { Helmet } from 'react-helmet';
import { any, string } from 'prop-types';

import { ViewStyled } from './ViewStyled';

const View = ({ title, children }) => (
	<ViewStyled>
		<Helmet>
			<title>{title}</title>
			<link rel="canonical" href={window.location.href} />
		</Helmet>
		{children}
	</ViewStyled>
);

View.propTypes = {
	children: any /* eslint-disable-line react/forbid-prop-types */,
	title: string.isRequired
};

export default View;
