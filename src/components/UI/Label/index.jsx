import React from 'react';
import { string } from 'prop-types';

import { LabelStyled } from './LabelStyled';

const Label = props => <LabelStyled {...props} />;

Label.propTypes = {
	htmlFor: string.isRequired
};

Label.defaultProps = {};

export default Label;
