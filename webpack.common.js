import { resolve } from 'path';
import { config } from 'dotenv';
import CopyWebpackPlugin from 'copy-webpack-plugin';
import CleanWebpackPlugin from 'clean-webpack-plugin';
import DotenvPlugin from 'dotenv-webpack';
import HtmlWebpackPlugin from 'html-webpack-plugin';
import MiniCssExtractPlugin from 'mini-css-extract-plugin';
import CspHtmlWebpackPlugin from 'csp-html-webpack-plugin';
// Current conflict between preload & SRI?
// https://github.com/w3c/preload/issues/127
import PreloadPlugin from 'preload-webpack-plugin';
import SubResourceIntegrityPlugin from 'webpack-subresource-integrity';

// Load environment variables to process.env
config();

export const publicDir = resolve(__dirname, 'public');
export const srcDir = resolve(__dirname, 'src');
export const assetsDir = resolve(srcDir, 'assets');
export const distDir = resolve(__dirname, 'dist');
export const tokensDir = resolve(__dirname, 'tokens');

export default {
	context: srcDir,
	entry: {
		app: './index.jsx'
	},
	output: {
		path: distDir,
		filename: '[name].[hash].js',
		publicPath: '/',
		crossOriginLoading: 'anonymous'
	},
	resolve: {
		extensions: [
			'.js',
			'.jsx',
			'.html',
			'.jpg',
			'.jpeg',
			'.png',
			'.gif',
			'.webp',
			'.svg',
			'.woff',
			'.woff2'
		],
		alias: {
			'~src': srcDir,
			'~tokens': tokensDir
		},
		symlinks: false
	},
	module: {
		rules: [
			{
				test: /\.jsx?$/,
				use: [
					{
						loader: 'babel-loader'
					}
				],
				exclude: /node_modules/
			},
			{
				test: /\.css$/,
				use: [
					['production'].includes(process.env.NODE_ENV)
						? MiniCssExtractPlugin.loader
						: 'style-loader',
					'css-loader',
					'postcss-loader'
				],
				exclude: [/node_modules/]
			},
			{
				test: /\.(jpe?g|png|gif|webp|svg|woff2?)$/,
				use: [
					{
						loader: 'file-loader',
						options: {
							name: '[path][name].[ext]',
							emitFile: true
						}
					}
				],
				include: assetsDir,
				exclude: /node_modules/
			}
		]
	},
	plugins: [
		new DotenvPlugin({
			safe: false, // do not match .env.example, we might not need all parameters to be defined in all environments
			systemvars: true // only load vars from '.env' file if they are NOT already defined as env variables
		}),
		new CleanWebpackPlugin(distDir, {
			exclude: ['.gitignore']
		}),
		new CopyWebpackPlugin([
			{
				from: publicDir,
				to: distDir,
				ignore: /index.html/ // Do not copy, it would be injected by HtmlWebpackPlugin
			}
		]),
		new HtmlWebpackPlugin({
			template: resolve(publicDir, 'index.html'),
			path: distDir,
			filename: 'index.html',
			minify: {
				collapseInlineTagWhitespace: true,
				collapseWhitespace: true,
				removeComments: true,
				removeRedundantAttributes: true
			}
		}),
		new SubResourceIntegrityPlugin({
			hashFuncNames: ['sha256', 'sha384'],
			enabled: process.env.NODE_ENV === 'production'
		}),
		new PreloadPlugin({
			rel: 'preload',
			include: 'allChunks'
		}),
		new CspHtmlWebpackPlugin(
			{
				'default-src': "'none'",
				'script-src': ["'self'", 'https://storage.googleapis.com/', 'https://cdn.polyfill.io/'],
				'style-src': ["'self'", "'unsafe-inline'"],
				'img-src': ["'self'", 'data:'],
				'font-src': "'self'",
				'media-src': "'self'",
				'manifest-src': "'self'",
				'worker-src': "'self'",
				'connect-src': "'self'",
				'object-src': "'none'",
				'frame-src': "'self'",
				'base-uri': "'none'"
			},
			{
				enabled: true,
				hashingMethod: 'sha256'
			}
		)
	]
};
