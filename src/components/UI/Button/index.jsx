import React from 'react';
import { bool } from 'prop-types';

import { ButtonStyled } from './ButtonStyled';

const Button = props => (
	<ButtonStyled {...props} />
);

Button.propTypes = {
	disabled: bool
};

Button.defaultTypes = {
	disabled: false
};

export default Button;
