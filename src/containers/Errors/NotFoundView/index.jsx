import React from 'react';
import { any, string } from 'prop-types';

import { SectionStyled } from './NotFoundViewStyled';

import View from '~src/containers/View';
import H1 from '~src/components/UI/H1';
import Paragraph from '~src/components/UI/Paragraph';

class NotFoundView extends React.Component {
	render() {
		const { title, children } = this.props;

		return (
			<View title="404 Page Not Found | PWA by Humblebee">
				<SectionStyled>
					<H1>{title}</H1>
					{children}
				</SectionStyled>
			</View>
		);
	}
}

NotFoundView.propTypes = {
	title: string,
	children: any /* eslint-disable-line react/forbid-prop-types */
};

NotFoundView.defaultProps = {
	title: 'Page Not Found',
	children: <Paragraph>The page you are looking for does not exists...</Paragraph>
};

export default NotFoundView;
