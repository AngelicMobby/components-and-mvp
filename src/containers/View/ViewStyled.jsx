import styled from 'styled-components';

export const ViewStyled = styled.div`
	width: 100%;
	max-width: 100%;
	height: auto;
	margin: 0 auto;
	box-sizing: border-box;
	overflow-x: hidden;
`;
