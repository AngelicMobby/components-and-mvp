import styled from 'styled-components';

import fontSizes from '~tokens/fontSizes.mjs';
import lineHeights from '~tokens/lineHeights.mjs';
import fontWeights from '~tokens/fontWeights.mjs';

export const H2Styled = styled.h2`
	font-size: ${fontSizes.h2};
	line-height: ${lineHeights.xs};
	font-weight: ${fontWeights.medium};
	text-align: ${({ center }) => (center ? 'center' : '')};
`;
