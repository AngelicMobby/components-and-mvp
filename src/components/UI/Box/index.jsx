import React from 'react';
import { bool } from 'prop-types';

import { BoxStyled } from './BoxStyled';

const Box = props => <BoxStyled {...props} />;

Box.propTypes = {
	marginTop: bool,
	shadow: bool
};

Box.defaultProps = {
	marginTop: false,
	shadow: false
};

export default Box;
