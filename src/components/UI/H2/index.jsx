import React from 'react';
import { bool } from 'prop-types';

import { H2Styled } from './H2Styled';

const H2 = props => <H2Styled {...props} />;

H2.propTypes = {
	center: bool
};

H2.defaultProps = {
	center: false
};

export default H2;
