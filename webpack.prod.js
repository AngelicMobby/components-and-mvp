import webpack from 'webpack';
import merge from 'webpack-merge';
import TerserPlugin from 'terser-webpack-plugin';
import BabelMinifyPlugin from 'babel-minify-webpack-plugin';
import MiniCssExtractPlugin from 'mini-css-extract-plugin';
import OptimizeCssAssetsPlugin from 'optimize-css-assets-webpack-plugin';
import ImageminPlugin from 'imagemin-webpack-plugin';
import ImageminMozjpeg from 'imagemin-mozjpeg';
import WorkboxPlugin from 'workbox-webpack-plugin';
import BrotliGzipPlugin from 'brotli-gzip-webpack-plugin';

import config from './webpack.common.js';

export default merge(config, {
	mode: 'production',
	plugins: [
		new webpack.HashedModuleIdsPlugin(),
		new webpack.optimize.ModuleConcatenationPlugin(),
		new BabelMinifyPlugin({
			removeConsole: true,
			removeDebugger: true
		}),
		new MiniCssExtractPlugin({
			filename: '[name].[contenthash].css',
			chunkFilename: '[id].[contenthash].css'
		}),
		new ImageminPlugin({
			svgo: {
				removeViewBox: false,
				removeDimensions: true
			},
			plugins: [
				ImageminMozjpeg({
					progressive: true,
					quality: 55
				})
			]
		}),
		new WorkboxPlugin.GenerateSW({
			clientsClaim: true,
			directoryIndex: 'index.html',
			navigateFallback: 'index.html',
			skipWaiting: true,
			swDest: 'sw.js'
		}),
		new BrotliGzipPlugin({
			asset: '[path].br[query]',
			algorithm: 'brotli',
			test: /\.(js|css|html|txt|xml|json|md|ico|jpe?g|png|gif|webp|svg|woff2?|webapp|webmanifest)$/,
			threshold: 1024,
			minRatio: 0.8
		}),
		new BrotliGzipPlugin({
			asset: '[path].gz[query]',
			algorithm: 'gzip',
			test: /\.(js|css|html|txt|xml|json|md|ico|jpe?g|png|gif|webp|svg|woff2?|webapp|webmanifest)$/,
			threshold: 1024,
			minRatio: 0.8
		})
	],
	optimization: {
		minimize: true,
		minimizer: [new TerserPlugin(), new OptimizeCssAssetsPlugin()],
		splitChunks: {
			chunks: 'all'
		}
	},
	performance: {
		maxEntrypointSize: 256000,
		maxAssetSize: 256000,
		hints: 'warning'
	}
});
