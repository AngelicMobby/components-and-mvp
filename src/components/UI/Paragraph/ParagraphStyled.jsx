import styled from 'styled-components';

import fontSizes from '~tokens/fontSizes.mjs';
import colors from '~tokens/colors.mjs';
import lineHeights from '~tokens/lineHeights.mjs';

export const ParagraphStyled = styled.p`
	font-size: ${fontSizes.m};
	line-height: ${lineHeights.m};
	color: ${colors.gray1};
`;
