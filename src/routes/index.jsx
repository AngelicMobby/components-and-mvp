import React from 'react';
import { Route, Switch } from 'react-router-dom';
import Loadable from 'react-loadable';

import Loading from './Loading';
// import AuthenticatedRoute from './AuthenticatedRoute';

/* Note: Loadable may not work correctly in SSR */
const HomeView = Loadable({
	loader: () => import('~src/containers/HomeView'),
	loading: Loading
});
const ExamplesView = Loadable({
	loader: () => import('~src/containers/ExamplesView'),
	loading: Loading
});
const FormView = Loadable({
	loader: () => import('~src/containers/FormView'),
	loading: Loading
});
const NotFoundView = Loadable({
	loader: () => import('~src/containers/Errors/NotFoundView'),
	loading: Loading
});

// Important: keep the router stateless!!
const Routes = () => (
	<Switch>
		<Route exact path="/" component={HomeView} />
		<Route exact path="/examples" component={ExamplesView} />
		<Route exact path="/form" component={FormView} />
		{/*
		<Route exact path="/login" component={LoginView} />
		<AuthenticatedRoute exact path="/profile" component={ProfileView} isAuthenticated={false} redirectTo={'/login'} />
		*/}
		<Route component={NotFoundView} />
	</Switch>
);

export default Routes;
