import React from 'react';
import { bool, string } from 'prop-types';

import Label from '~src/components/UI/Label';

import { TextareaWrapperStyled, TextareaStyled } from './TextareaStyled';

const Textarea = ({ name, label, ...props }) => (
	<TextareaWrapperStyled>
		<Label htmlFor={name}>{label}</Label>
		<TextareaStyled id={name} name={name} {...props} />
	</TextareaWrapperStyled>
);

Textarea.propTypes = {
	label: string,
	name: string.isRequired,
	required: bool,
	inline: bool
};

Textarea.defaultProps = {
	label: 'Some input',
	required: false,
	inline: false
};

export default Textarea;
