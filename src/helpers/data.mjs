export const getData = async (endpoint, options) => {
	let data = undefined;

	try {
		const response = await fetch(endpoint, options);
		data = response.json();
	} catch (error) {} /* eslint-disable-line no-empty */

	return data;
};

export const sendData = async (endpoint, options) => {
	let data = undefined;

	try {
		const response = await fetch(endpoint, options);
		data = response.json();
	} catch (error) {} /* eslint-disable-line no-empty */

	return data;
};
